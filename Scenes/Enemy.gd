extends KinematicBody2D

export(String) var scene_to_load
onready var animator = self.get_node("AnimationPlayer")
var ENEMY_ATTACK_TIME = 5
var enemy_attack_countdown = ENEMY_ATTACK_TIME
var enemy_life_start = 100
var enemy_life = enemy_life_start
var hard_mode := false
signal health_signal
onready var Enemy_Attack = preload("res://Scenes/Enemy_Attack.tscn")

var array_number = [0,1,2]
var animate = "idle"

func _ready():
	pass

func _process(delta):
	emit_signal("health_signal", enemy_life)
	if animate == "attack":
		animator.play("attack")
	elif animate == "death":
		animator.play("Death")
	else:	
	 animator.play('idle')

func eject_fireball():
	pass

func hit():
	enemy_life -= 50
	emit_signal("health_signal", enemy_life)
	get_parent().get_node("Gate1").visible = true
	get_parent().get_node("Gate2").visible = true
	get_parent().get_node("Gate3").visible = true
	get_node("Idle").modulate = Color(10,10,10,10)
	yield(get_tree().create_timer(0.2), "timeout")
	get_node("Idle").modulate = Color(1,1,1,1)
	yield(get_tree().create_timer(0.2), "timeout")
	get_node("Idle").modulate = Color(10,10,10,10)
	yield(get_tree().create_timer(0.2), "timeout")
	get_node("Idle").modulate = Color(1,1,1,1)
	yield(get_tree().create_timer(0.2), "timeout")
	if self.get_parent().get_node("FireSpawner").hasspawn == false:
		animate = "attack"
		yield( animator, "animation_finished")
		animate = "idle"
		self.get_parent().get_node("FireSpawner").hasspawn == true
		self.get_parent().get_node("FireSpawner").spawn = true
	if hard_mode == false:
		print("not yet")
		if enemy_life <= 0:
			hard_mode = true
			enemy_life = enemy_life_start
	
	if hard_mode == true:
		if enemy_life <= 0:
				self.get_parent().get_node("FireSpawner").spawn = false
				self.get_node("Camera2D").current = true
				animate = "death"
				yield( animator, "animation_finished")
				get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
			
		animate = "attack"
		yield( animator, "animation_finished" )
		animate = "idle"
		array_number.shuffle()
		var my_random_number = array_number[0]
		array_number.remove(0)
		var trap_activate = "Trap3"
		if my_random_number == 1:
			trap_activate = "Trap1"
		elif my_random_number == 2:
			trap_activate = "Trap2"
		else:
			trap_activate = "Trap3"
		get_parent().get_node(trap_activate).modulate = Color(10,10,10,10)
		yield(get_tree().create_timer(0.2), "timeout")
		get_parent().get_node(trap_activate).modulate = Color(1,1,1,1)
		yield(get_tree().create_timer(0.2), "timeout")
		get_parent().get_node(trap_activate).modulate = Color(10,10,10,10)
		yield(get_tree().create_timer(0.2), "timeout")
		get_parent().get_node(trap_activate).modulate = Color(10,10,10,10)
		yield(get_tree().create_timer(0.2), "timeout")
		get_parent().get_node(trap_activate).modulate = Color(1,1,1,1)
		yield(get_tree().create_timer(0.2), "timeout")
		get_parent().get_node(trap_activate).modulate = Color(10,10,10,10)
		yield(get_tree().create_timer(0.2), "timeout")
		get_parent().get_node(trap_activate).modulate = Color(10,10,10,10)
		yield(get_tree().create_timer(0.2), "timeout")
		get_parent().get_node(trap_activate).modulate = Color(1,1,1,1)
		yield(get_tree().create_timer(0.2), "timeout")
		get_parent().get_node(trap_activate).modulate = Color(10,10,10,10)
		yield(get_tree().create_timer(0.2), "timeout")
		get_parent().get_node(trap_activate).get_node("TileMap").set_collision_mask(32)
