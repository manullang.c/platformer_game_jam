extends TileMap

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func hit():
	self.get_parent().get_parent().get_node("FireSpawner2").spawn = true
	self.get_parent().get_parent().get_node("TutorialElements/dodge").percent_visible = 1
	queue_free()