extends KinematicBody2D

export (int) var projectile_speed = 200
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var VELOCITY = Vector2(projectile_speed, 0)
onready var projectile_sprite = self.get_node("Sprite")
var is_flipped
onready var animator = self.get_node("AnimationPlayer")
# Called when the node enters the scene tree for the first time.
func _ready():
	if is_flipped == 1:
		VELOCITY.x -= 400
	else:
 		projectile_sprite.flip_h = true
	animator.play("idle")

func set_flip(is_flipped):
	print("flip is now set")
	self.is_flipped = is_flipped
    

func hit(object):
    if object.name == TileSet:
        queue_free()

# Moves the actual projectile
func move(delta):
    var collision = move_and_collide(VELOCITY * delta)
    if collision:
        if collision.collider.has_method("hit"):
            collision.collider.hit()
        queue_free()

# Deletes the projectile when it goes off screen
func removeWhenOffScreen():
    if global_position.y < 0:
        queue_free()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
    move(delta)
    removeWhenOffScreen()

