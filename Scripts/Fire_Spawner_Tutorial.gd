extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var Fireball = preload("res://Scenes/Enemy_Attack.tscn")
var charge = 0
var spawn = false
# Called when the node enters the scene tree for the first time.

func _ready():
	pass # Replace with function body.
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	charge += 0.1
	if charge >= 10 && spawn == true:
		charge = 0
		var fireball_instance = Fireball.instance()
		fireball_instance.global_position = global_position
		get_parent().add_child(fireball_instance)
		
		
		
		
