extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var Fireball = preload("res://Scenes/Enemy_Attack.tscn")
# Called when the node enters the scene tree for the first time.

var spawn = false
var spawnfirst = true
var hasspawn = false
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if spawn:
		spawn = false
		while true:
			var fireball_instance = Fireball.instance()
			var rng = RandomNumberGenerator.new()
			rng.randomize()
			var my_random_number = rng.randf_range(-150, 600)
			fireball_instance.global_position = global_position + Vector2(my_random_number,0)
			get_parent().add_child(fireball_instance)
			if spawnfirst:
				spawnfirst = false
				fireball_instance.get_node("Camera2D").current = true
				yield(get_tree().create_timer(1), "timeout")
				get_node("/root/Node2D/Player/Camera2D").current = true
			else:
				yield(get_tree().create_timer(1), "timeout")
		
		
		
