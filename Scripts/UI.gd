extends MarginContainer

onready var enemy_health_bar = get_node("HBoxContainer").get_node("Life Bar2").get_node("TextureProgress")
onready var player_charge_bar = get_node("HBoxContainer").get_node("Life Bar").get_node("TextureProgress")
onready var Player = get_node("/root/Node2D/Player")
onready var Enemy = get_node("../../Enemy")

onready var tween = $Tween

var animated_charge = 0
var animated_enemy_health = 0

func _ready():
	player_charge_bar.max_value = Player.get("CHARGE_TIME")
	enemy_health_bar.max_value = Enemy.get("enemy_life_start")

func _on_Enemy_health_signal(enemy_life):
	update_enemy_health(enemy_life)

func _on_Player_charging_signal(charging):
	update_charge(charging)	
	
func _process(delta):
	player_charge_bar.value = animated_charge
	enemy_health_bar.value = animated_enemy_health
	
func update_charge(new_value):
	tween.interpolate_property(self, "animated_charge", animated_charge, new_value, 0.01, Tween.TRANS_LINEAR, Tween.EASE_IN)
	if not tween.is_active():
		tween.start()

func update_enemy_health(new_value):
	tween.interpolate_property(self, "animated_enemy_health", animated_enemy_health, new_value, 0.01, Tween.TRANS_LINEAR, Tween.EASE_IN)
	if not tween.is_active():
		tween.start()
	



