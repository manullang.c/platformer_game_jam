extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var label_revealed = "beware"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	if body == self.get_parent().get_parent().get_node("Player"):
		destroy_floor()
		self.get_parent().get_parent().get_node("TutorialElements").get_node("beware").percent_visible = 1
		yield(get_tree().create_timer(0.2), "timeout")
		self.get_parent().get_parent().get_node("TutorialElements").get_node("beware").percent_visible = 0
		yield(get_tree().create_timer(0.2), "timeout")
		self.get_parent().get_parent().get_node("TutorialElements").get_node("beware").percent_visible = 1

func destroy_floor():
	var trap_activate = "Destroyable_trap"
	get_parent().get_node(trap_activate).modulate = Color(10,10,10,10)
	yield(get_tree().create_timer(0.2), "timeout")
	get_parent().get_node(trap_activate).modulate = Color(1,1,1,1)
	yield(get_tree().create_timer(0.2), "timeout")
	get_parent().get_node(trap_activate).modulate = Color(10,10,10,10)
	yield(get_tree().create_timer(0.2), "timeout")
	get_parent().get_node(trap_activate).modulate = Color(10,10,10,10)
	yield(get_tree().create_timer(0.2), "timeout")
	get_parent().get_node(trap_activate).modulate = Color(1,1,1,1)
	yield(get_tree().create_timer(0.2), "timeout")
	get_parent().get_node(trap_activate).modulate = Color(10,10,10,10)
	yield(get_tree().create_timer(0.2), "timeout")
	get_parent().get_node(trap_activate).modulate = Color(10,10,10,10)
	yield(get_tree().create_timer(0.2), "timeout")
	get_parent().get_node(trap_activate).modulate = Color(1,1,1,1)
	yield(get_tree().create_timer(0.2), "timeout")
	get_parent().get_node(trap_activate).modulate = Color(10,10,10,10)
	yield(get_tree().create_timer(0.2), "timeout")
	get_parent().get_node(trap_activate).set_collision_mask(32)
