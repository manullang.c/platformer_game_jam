extends MarginContainer

export(String) var scene_to_load


func _ready():
	yield(get_tree().create_timer(5), "timeout")
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))