extends StaticBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		get_parent().get_node("Gate4").get_node("Camera2D").current = true

		yield(get_tree().create_timer(0.5), "timeout")
		get_parent().get_node("Gate4").visible = false
		get_parent().get_node("Gate4").collision_mask = 32
		yield(get_tree().create_timer(0.5), "timeout")
		get_parent().get_node("Gate4").get_node("Camera2D").current = false
		get_parent().get_node("Player").get_node("Camera2D").current = true
		yield(get_tree().create_timer(10), "timeout")
		get_parent().get_node("Gate4").visible = true
		get_parent().get_node("Gate4").collision_mask = 1


		
	
