extends KinematicBody2D

export (int) var projectile_speed = 100
var VELOCITY = Vector2(0, projectile_speed)
onready var projectile_sprite = self.get_node("Sprite")
onready var animator = self.get_node("AnimationPlayer")

func _ready():
	animator.play("idle")
	pass
    
func hit():
	queue_free()

# Moves the actual projectile
func move(delta):
	var collision = move_and_collide(VELOCITY * delta)



# Deletes the projectile when it goes off screen
func removeWhenOffScreen():
    if global_position.y < 0:
        queue_free()

func _physics_process(delta):
    move(delta)

func _process(delta):
	if get_parent().get("spawn") == false:
		queue_free()

func _on_Area2D_body_entered(body):
	if body == get_parent().get_node("Player"):
		body.hit()

	
