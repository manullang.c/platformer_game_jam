extends KinematicBody2D
export (int) var speed = 400
export (int) var GRAVITY = 2000
export (int) var jump_speed = -400
export(String) var scene_to_load
const UP = Vector2(0,-1)
const CHARGE_TIME = 5
signal charging_signal
var velocity = Vector2()
var charging = CHARGE_TIME
var is_flipped : = 0


onready var Fireball = preload("res://Scenes/Player_Attack.tscn")
onready var animator = self.get_node("AnimationPlayer")
onready var run = self.get_node("Run")
onready var fire = self.get_node("Fire")
onready var idle = self.get_node("Idle")

func get_input():
    velocity.x = 0
    if is_on_floor() and Input.is_action_just_pressed('jump'):
        velocity.y = jump_speed
    if Input.is_action_pressed('right'):
        velocity.x += speed
    if Input.is_action_pressed('left'):
        velocity.x -= speed
    if Input.is_action_just_pressed('fire'):
        if charging >= CHARGE_TIME:
            shoot()
            charging = 0
            emit_signal("charging_signal", charging)

func hit():
	queue_free()
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _process(delta):
    charging += delta
    emit_signal("charging_signal", charging)
    if velocity.x != 0:
        animator.play("run")
        if velocity.x > 0:
            run.flip_h = false
            idle.flip_h = false
            fire.flip_h = false
            is_flipped = 0
        else:
            run.flip_h = true
            idle.flip_h = true
            fire.flip_h = true
            is_flipped = 1
    else:
        animator.play("idle")

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func shoot():
    var fireball_instance = Fireball.instance()
    fireball_instance.global_position = global_position
    fireball_instance.set_flip(is_flipped)
    get_parent().add_child(fireball_instance)
    



